data "google_iam_policy" "storage_admin" {
  binding {
    role = var.storage_role

    members = [
      "serviceAccount:${var.service_account_id}",
    ]
  }
}

data "google_iam_policy" "owner" {
  binding {
    role = var.owner_role

    members = [
      "serviceAccount:${var.service_account_id}",
    ]
  }
}

data "google_iam_policy" "sevice_acount_admin" {
  binding {
    role = var.serviceaccount_role

    members = [
      "serviceAccount:${var.service_account_id}",
    ]
  }
}

data "google_iam_policy" "bigquery_admin" {
  binding {
    role = var.bigquery_role

    members = [
      "serviceAccount:${var.service_account_id}",
    ]
  }
}

