variable "dataset_id" {
  default = "Car_Name"
}

variable "bq_friendly_name" {
    default = "Bike"
}

variable "location" {
    default = "eu"
}

variable "table_name" {
    default = "Data" 
}

variable "bucket_name"{
    default = "play-book-678"
}

variable "project_id" {
  default = "my-project-49407"
}

variable "service_account_id" {
    default = "hellobook"
}

variable "name_description" {
    default = "Play_Book" 
}

variable "storage_role" {
  
  description = "The roles that will be granted to the service account."
  default     = "storage.admin"
}
variable "owner_role" {
  
  description = "The roles that will be granted to the service account."
  default     = "project.owner"
}


variable "serviceaccount_role" {
 
  description = "The roles that will be granted to the service account."
  default     = "iam.serviceAccountUser"
}

variable "bigquery_role" {
  
  description = "The roles that will be granted to the service account."
  default     = "bigquery.admin" 
}


variable "networkmanagement_role" {
  description = "The roles that will be granted to the service account."
  default     = "networkmanagement.admin"
}

variable "test_firewall" {
  default = "playbook-firewall"
}


variable "network-vpc" {
    default = "secure-network"
  
}
variable "vpc-subnet" {
    default = "lan-cable"
}   
variable "ip-range"{
  default = "10.2.0.0/24"
}

