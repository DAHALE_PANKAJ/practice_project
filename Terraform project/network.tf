
resource "google_compute_network" "vpc-network" {
name = var.network-vpc
auto_create_subnetworks = "false"


}
resource "google_compute_subnetwork" "public-subnetwork" {
name = var.vpc-subnet
ip_cidr_range = var.ip-range
region = "us-central1"
network = google_compute_network.vpc-network.name
}
