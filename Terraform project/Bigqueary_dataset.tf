# DataSet Creation Block

resource "google_bigquery_dataset" "default" {
  dataset_id                  = var.dataset_id
  friendly_name               = var.bq_friendly_name
  location                    = var.location
  delete_contents_on_destroy  = true
}

#DataSet Table Creation Block 

resource "google_bigquery_table" "default" {
  dataset_id = var.dataset_id
  table_id   = var.table_name 
  depends_on = [google_bigquery_dataset.default]
  deletion_protection  = false
  schema = <<EOF
[
  {
    "name": "No_of_Bikes",
    "type": "INTEGER",
    "mode": "NULLABLE",
    "description": "Total no. of bikes dispach"
  },
  {
    "name": "Model_Name",
    "type": "STRING",
    "mode": "NULLABLE",
    "description": "Modal name of bike."
  }
]
EOF

}
