resource "google_compute_firewall" "default" {
  name    = var.test_firewall
  network = google_compute_network.vpc-network.name

  allow {
    protocol = "icmp"
  }

  allow {
    protocol = "tcp"
    ports    = ["80", "8080", "1000-2000"]
  }

  source_tags = ["web"]
}

